variable "kinesis_stream_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}

variable "kinesis_stream_name" {
  description = "(Required) A name to identify the stream. This is unique to the AWS account and region the Stream is created in."
  type        = string
}
variable "kinesis_stream_tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

variable "kinesis_stream_shard_count" {
  description = "The number of shards that the stream will use."
  # Amazon has guidelines for specifying the Stream size that should be referenced when creating a Kinesis stream. See:
  #   * https://docs.aws.amazon.com/kinesis/latest/dev/amazon-kinesis-streams.html
  type        = number
  default     = 1
}

variable "kinesis_stream_retention_period" {
  description = "Length of time data records are accessible after they are added to the stream."
  # Must be between 24 and 168 hours.
  type        = number
  default     = 24
}

variable "kinesis_stream_shard_level_metrics" {
  description = "A list of shard-level CloudWatch metrics which can be enabled for the stream."
  # See Monitoring with CloudWatch for more:
  #   * https://docs.aws.amazon.com/streams/latest/dev/monitoring-with-cloudwatch.html
  # NOTE(!): that the value "ALL" should not be used; instead you should provide an explicit list of metrics
  # you wish to enable.
  type        = list(string)
  default     = []
}

variable "kinesis_stream_enforce_consumer_deletion" {
  description = "A boolean that indicates all registered consumers should be deregistered from the stream so that the stream can be destroyed without error."
  type        = bool
  default     = false
}

variable "kinesis_stream_encryption_type" {
  description = "The encryption type to use. The only acceptable values are NONE or KMS."
  type        = string
  default     = "NONE"
}
variable "kinesis_stream_kms_key_id" {
  description = "The GUID for the customer-managed KMS key to use for encryption. You can also use a Kinesis-owned master key by specifying the alias `alias/aws/kinesis`."
  type        = string
  default     = ""
}
