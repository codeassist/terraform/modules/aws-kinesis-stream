locals {
  enabled = var.kinesis_stream_module_enabled ? true : false
  tags = merge(
    var.kinesis_stream_tags,
    {
      Name      = format("%s", var.kinesis_stream_name)
      terraform = "true"
    },
  )
}

resource "aws_kinesis_stream" "this" {
  # Provides a Kinesis Stream resource. Amazon Kinesis is a managed service that scales elastically for real-time
  # processing of streaming big data.
  # For more details, see the Amazon Kinesis Documentation:
  #   * https://aws.amazon.com/documentation/kinesis/
  count = local.enabled ? 1 : 0

  # (Required) A name to identify the stream. This is unique to the AWS account and region the Stream is created in.
  name = var.kinesis_stream_name
  # (Required) The number of shards that the stream will use. Amazon has guidelines for specifying the Stream size
  # that should be referenced when creating a Kinesis stream.
  shard_count = var.kinesis_stream_shard_count
  # Length of time data records are accessible after they are added to the stream.
  retention_period = var.kinesis_stream_retention_period
  # A list of shard-level CloudWatch metrics which can be enabled for the stream.
  shard_level_metrics = var.kinesis_stream_shard_level_metrics
  # A boolean that indicates all registered consumers should be deregistered from the stream so that the stream
  # can be destroyed without error.
  enforce_consumer_deletion = var.kinesis_stream_enforce_consumer_deletion
  # The encryption type to use.
  encryption_type = var.kinesis_stream_encryption_type
  # The GUID for the customer-managed KMS key to use for encryption.
  kms_key_id = var.kinesis_stream_kms_key_id
  # A mapping of tags to assign to the resource.
  tags = local.tags
}
