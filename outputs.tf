output "kinesis_stream_name" {
  description = "The unique Stream Name."
  value       = join("", aws_kinesis_stream.this.*.name)
}

output "kinesis_stream_shard_count" {
  description = "The count of Shards for this Stream."
  value       = join("", aws_kinesis_stream.this.*.shard_count)
}

output "kinesis_stream_arn" {
  description = "The Amazon Resource Name (ARN) specifying the Stream (same as ID)."
  value       = join("", aws_kinesis_stream.this.*.arn)
}
